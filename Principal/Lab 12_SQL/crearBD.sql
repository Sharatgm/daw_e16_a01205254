IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales') 

DROP TABLE Materiales 

create table Materiales 
( 
  Clave numeric(5) not null, 
  Descripcion varchar(50), 
  Costo numeric (8,2) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores') 

DROP TABLE Proveedores 


create table Proveedores 
( 
  RFC char(13) not null, 
  RazonSocial varchar(50) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos') 

DROP TABLE Proyectos


create table Proyectos 
( 
  Numero numeric(5) not null, 
  Denominacion varchar(50) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan') 

DROP TABLE Entregan

create table Entregan 
( 
  Clave numeric(5) not null, 
  RFC char(13) not null, 
  Numero numeric(5) not null, 
  Fecha DateTime not null, 
  Cantidad numeric (8,2) 
) 

BULK INSERT a1205254.a1205254.[Materiales] 
  FROM 'e:\wwwroot\a1271656\materiales.csv' 
  WITH 
  ( 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT a1205254.a1205254.[Proyectos] 
  FROM 'e:\wwwroot\a1271656\Proyectos.csv' 
  WITH 
  ( 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT a1205254.a1205254.[Proveedores] 
  FROM 'e:\wwwroot\a1271656\Proveedores.csv' 
  WITH 
  ( 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

SET DATEFORMAT dmy -- especificar formato de la fecha 

BULK INSERT a1205254.a1205254.[Entregan] 
  FROM 'e:\wwwroot\a1271656\Entregan.csv' 
  WITH 
  ( 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 
