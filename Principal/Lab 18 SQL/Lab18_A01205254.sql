/*--------------------------------------------------------
Materiales(Clave, Descripci�n, Costo, PorcentajeImpuesto) 
Proveedores(RFC, RazonSocial) 
Proyectos(Numero, Denominacion) 
Entregan(Clave, RFC, Numero, Fecha, Cantidad) 
---------------------------------------------------------*/

-- 1. Con base en lo que se explica en la lectura sobre funciones agregadas, plantea y ejecuta las siguientes consultas, agregando 
-- los alias de columna necesarios para que los resultados resulten legibles: 

-- La suma de las cantidades e importe total de todas las entregas realizadas durante el 97.

SET DATEFORMAT dmy
SELECT SUM(Cantidad) As Total, SUM(Costo + PorcentajeImpuesto * Costo) As 'Importe'
FROM  Materiales M, Entregan E
WHERE M.Clave = E.Clave AND E.Fecha BETWEEN '01/01/1999' AND '31/12/1999'

-- Para cada proveedor, obtener la raz�n social del proveedor, n�mero de entregas e importe total de las entregas realizadas. 

 SELECT RazonSocial, SUM(Numero) As 'TotalEntregas', SUM(Costo + PorcentajeImpuesto * Costo) As 'TotalImporte'
 FROM Materiales M, Entregan E, Proveedores P
 WHERE P.RFC = E.RFC AND M.Clave = E.Clave
 GROUP BY RazonSocial

 -- Por cada material obtener la clave y descripci�n del material, la cantidad total entregada, la m�nima cantidad entregada, la
 -- m�xima cantidad entregada, el importe total de las entregas de aquellos materiales en los que la cantidad promedio entregada sea 
 --mayor a 400.

 SELECT M.Clave, Descripcion, SUM(Cantidad) As 'TotalEntregas', MIN(Cantidad) As 'CantidadMinima', MAX(Cantidad) As 'CantidadMaxima', SUM(Costo + PorcentajeImpuesto*Costo) As 'Importe'
 FROM Materiales M, Entregan E
 WHERE M.Clave = E.Clave
 GROUP BY M.Clave, Descripcion 
 HAVING AVG(Cantidad) > 400

 -- Para cada proveedor, indicar su raz�n social y mostrar la cantidad promedio de cada material entregado, detallando la clave y 
 -- descripci�n del material, excluyendo aquellos proveedores para los que la  cantidad promedio sea menor a 500. 

 SELECT RazonSocial, AVG(Cantidad) As 'Cantidad Promedio', M.Clave, Descripcion
 FROM Materiales M, Entregan E, Proveedores P
 WHERE M.Clave = E.Clave
 GROUP BY RazonSocial, M.Clave, Descripcion
 HAVING AVG(Cantidad) >= 500

 -- Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos grupos de proveedores: aquellos para los que
 -- la cantidad promedio entregada es menor a 370 y aquellos para los que la cantidad promedio entregada sea mayor a 450.

 SELECT RazonSocial, AVG(Cantidad) As 'Cantidad Promedio', M.Clave, Descripcion
 FROM Materiales M, Proveedores P, Entregan E
 WHERE M.Clave = E.Clave
 GROUP BY RazonSocial, M.Clave, Descripcion
 HAVING AVG(Cantidad) NOT BETWEEN 370 AND 450
 
-- 2. Utilizando la sentencia INSERT INTO tabla VALUES (valorcolumna1, valorcolumna2, [...] , valorcolumnan) ; 

 INSERT INTO Materiales VALUES (1440 'Fierro', 450, 1.5 )
 INSERT INTO Materiales VALUES (1450, 'Yeso', 200, 1.5 )
 INSERT INTO Materiales VALUES (1460, 'Arena', 140, 1.5 )
 INSERT INTO Materiales VALUES (1470, 'Acerrin', 98, 1.5 )
 INSERT INTO Materiales VALUES (1480, 'Gravilla', 120, 1.5 )
 
 -- 3. Con base en lo que se explica en la lectura sobre consultas con roles y subconsultas, plantea y ejecuta las siguientes consultas: 

 -- Clave y descripci�n de los materiales que nunca han sido entregados. 
 SELECT Clave, Descripcion
 FROM Materiales
 WHERE Clave NOT IN( SELECT Clave FROM Entregan)

 --Raz�n social de los proveedores que han realizado entregas tanto al proyecto 'Vamos M�xico' como al proyecto 'Quer�taro Limpio'. 

  SELECT RazonSocial
  FROM Proveedores P, Proyectos Pr, Entregan E
  WHERE  Pr.Numero = E.Numero AND P.RFC = E.RFC AND Denominacion IN (  SELECT Denominacion
  FROM Proyectos WHERE Denominacion = 'Vamos Mexico' OR Denominacion = 'Queretaro Limpio')
  GROUP BY RazonSocial

  -- Descripci�n de los materiales que nunca han sido entregados al proyecto 'CIT Yucat�n'. 

	SELECT Descripcion
   FROM Materiales, Proyectos
   WHERE Numero NOT IN(SELECT Numero FROM Proyectos WHERE Denominacion = 'CIT Yucat�n')

  -- Raz�n social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad entregada es mayor al promedio de la
  -- cantidad entregada por el proveedor con el RFC 'VAGO780901'. */

	SELECT RazonSocial, AVG(Cantidad) As 'Promedio'
	FROM Proveedores P, Entregan E
	WHERE E.RFC = P.RFC
	GROUP BY RazonSocial
	HAVING AVG(Cantidad) > ( SELECT AVG(Cantidad) FROM Proveedores P, Entregan E WHERE P.RFC = E.RFC AND P.RFC = 'VAGO780901')

  -- RFC, raz�n social de los proveedores que participaron en el proyecto 'Infonavit Durango' y cuyas cantidades totales entregadas 
  -- en el 2000 fueron mayores a las cantidades totales entregadas en el 2001. 

	SELECT E.RFC, RazonSocial
	FROM Materiales M, Proveedores P, Entregan E, Proyectos Pr
	WHERE Pr.Numero = E.Numero AND E.RFC = P.RFC AND Denominacion = 'Infonavit Durango' AND Fecha BETWEEN '01/01/2000' AND '31/12/2000' 
	GROUP BY E.RFC, RazonSocial
	HAVING SUM(Cantidad) >( SELECT SUM(Cantidad)From Entregan WHERE Fecha BETWEEN '01/01/2001' AND '31/12/2001')

