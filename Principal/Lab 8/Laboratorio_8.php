<!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
      <link rel="stylesheet" type= "text/css" href="../Stylesheet_DAW_BD.css"/>
   <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type= "text/css" href="Stylesheet_laboratorio_8.css"/>
   
    
</head>
<body>
    
    <main>
        <section  class = "title_box2 title_background2">
            
            <!--- DROPDOWN MENU ------>
            
            <nav>
                <div class="dropdown">
                      <button class="dropbtn2"><i class="fa fa-bars w3-text-white w3-xxxlarge"></i></button>     
                      <div class="dropdown-content2">
                       <a href="DAW y BD.html">Inicio</a>
                        <a href="Lab 1/Laboratorio_1.html">HTML 5</a>
                        <a href="Lab 3/Laboratorio_3.html">CSS</a>
                        <a href="Lab 4/Laboratorio_4.html">JavaScript</a>
                        <a href="Lab 5/Laboratorio_5.html">Programación Orientada a Eventos</a>
                        <a href="Lab 6/Laboratorio_6.html">Documentos Dinámicos con JavaScript</a>
                        <a href="Lab 7/Laboratorio_7.html">Front-end Frameworks</a>
                        <a href="Lab 8/Laboratorio_8.php">Introducción a PHP</a>
                        <a href="Lab 11/Laboratorio_11.php">Formas con PHP y modelo en capas</a>
                        <a href="Lab 13/Formulario.php">Manejo de Sesiones PHP</a>
                        
                        <a href="BD/Investiacion_1.html">Investigación 1</a>
                        <a href="Cuestionario_BD_DBMS/Cuestionario_BD_DBMS.html">Cuestionario BD &amp DBMS</a>
                    

                      </div>
                    </div>
            </nav> 
            
            <!--------TITLE--------->
            
            <div class="title_square2">
                <h1 class = "title fadeInDown">Introducción a PHP</h1>
            </div>
        </section>  
         
        <section>
            <div class = "descripcion">
                <h1>Descripción</h1>
                <p>En este laboratorio aprenderemos a usar PHP</p>
            </div>
        </section>
        <section>
            <div class= "funciones">
                <h1>Funciones</h1>
             <?php
                $arr = array(1, 8, 3, 24, 33, 8, 1, 0, 99);
                echo "<h3>Banco de datos 1</h3>";
                echo "<br>";

                print_r($arr);
                echo "<br>";
                echo "<h4>Funcion 1<h4>";
                promedio($arr);
                echo "<h4>Funcion 2<h4>";
                mediana($arr);
                echo "<h4>Funcion 3<h4>";
                lista_numeros($arr);
                
                $arr2 = array(33, 4, 56, 72, 98, 100, 3, 23, 43, 47, 11, 10, 87);
                echo "<h3>Banco de datos 2<h3>";
                echo "<br>";
                print_r($arr2);
                echo "<br>";
                echo "<h4>Funcion 1<h4>";
                promedio($arr2);
                echo "<h4>Funcion 2<h4>";
                mediana($arr2);
                echo "<h4>Funcion 3<h4>";
                lista_numeros($arr2);
        
                function promedio($array) {
                    $arrlength = count($array);
                    $i;
                    $aux = 0;
                    for ($i = 0; $i < $arrlength; $i++) {
                        $aux = $aux + $array[$i];
                    }
                    echo "El promedio es " . $aux/$arrlength;
                    echo "<br>";


                } 
                
                        function mediana($array) {
                    $arrlength = count($array);
                    asort($array);
                    $i;
                    $aux;
                    if($arrlength%2 == 0) {
                        $aux = (($array[floor($arrlength/2)]+$array[floor($arrlength/2) +1])/2);
                        echo "La mediana es " . $aux;
                        echo "<br>";
                    }
                    echo "La mediana es " . $array[floor($arrlength/2)+1];
                    echo "<br>";

                }
           

                    function lista_numeros($array) {
                        print_r($array);
                        echo "<ul>";
                        echo "<li>";
                        echo promedio($array);
                        echo "</li>";
                        echo "<li>";
                        echo mediana($array); 
                        echo "</li>";
                        echo "</ul>";
                        echo "Arreglo Ordenado de menor a mayor";
                        echo "<br>";
                        $arr = asort($array);
                        echo "<ul>";
                        foreach ($array as $a) {
                            echo "<li>";
                            echo $a;
                            echo "</li>";
                        }
                        echo "</ul>";
                         echo "Arreglo Ordenado de mayor a menor";
                        echo "<br>";
                        arsort($array);
                        echo "<ul>";
                        foreach ($array as $a) {
                            echo "<li>";
                            echo $a;
                            echo "</li>";
                        }
                        echo "</ul>";


                    }

                ?>
            <div>
                <h4>Funcion 4</h4>
                <p>Valores de ejemplo: 7 y 15</p>
               
                <?php
                    $numero = 7;
                    tabla($numero);
                    $numero2 = 15;
                    tabla($numero2);
                    
                    function tabla($numero) {
                    $i;
                    $j;
                    echo "<table>";
                    echo "<tr>";
                    echo "<td>Numero</td>";
                    echo "<td>Cuadrado</td>";
                    echo "<td>Cubo</td>";
                    echo "</tr>";
                    for($i = 0; $i < $numero; $i++) {
                        echo "<tr>";
                        for ($j = 1; $j <= 3; $j++) {
                            echo "<td>";
                            echo pow($i, $j);   
                            echo "</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</table>";
                        echo "<br>";
    
                    }
                   ?>
                </div>
                
                <div>
                <h4>Funcion 5</h4>
                <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                   Inserta un palíndromo <input type="text" name="palindromo">
                   <input type="submit">
                </form>

                <?php
                echo "<br>";    
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                     
                     $pal = $_REQUEST['palindromo']; 
                     if (empty($pal)) {
                         echo "No escribiste nada";
                     } else {
                         echo strrev($pal);
                     }
                    echo "Un palíndromo es aquella oración que invertida, dirá lo mismo que la original";
                    echo "<br>";
                }
                ?>
                </div>
            </div>
        </section>
        <section>
            <div class = "preguntas">
                <h1><i class = "fa fa-question"></i>    Preguntas</h1>
                
                <h3>¿Qué hace la función phpinfo()?</h3>
                <p> 
                   Esta función muestra información del estado actual de PHP como las opciones de compilación, la versión de PHP, muestra información sobre el servidor, rutas, licencia PHP, entre otros.
                    
                </p>
                
                <h3>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</h3>
                <p>Se recomienda que PHP esté corriendo en su propio espacio de memoria de manera que no existan deficiencias en la rappide y eficacia de PHP.</p>
                
                <h3>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor?</h3>
                <p>Porque al utilizar <?php y ?> para indicar el comienzo y término del código PHP, en el momento en que entra a esa parte del código todo se realiza en la parte del servidor hasta que se sale del "modo PHP" y se regresa al código HTML. Se encuentra en código HTML pero indicamos que es PHP para que se ejecute en el servidor.</p>
                
            
            </div>
            <div class = "referencias">
             <h1>REFERENCIAS</h1>
    
               <a>N.A. (s.f.). PHP and Text Boxes on HTML Forms. 01 de Marzo de 2016, de Home and Learn Sitio web: http://www.homeandlearn.co.uk/php/php4p6.html</a><br>
                <a>N.A.. (s.f.). phpinfo. 01 de Marzo de 2016, de PHP Sitio web: http://php.net/manual/es/function.phpinfo.php</a> <br>
                <a>N.A.. (s.f.). Instalación. 01 de Marzo de 2016, de PHP Sitio web: http://php.net/manual/es/faq.installation.php#faq.installation.apache2</a>
              
            
            </div>
        </section>
    </main>
  <script type = "text/javascript" src = "../Js_DAW_BD.js"></script>
  
</body>
</html>
