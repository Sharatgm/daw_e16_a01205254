CREATE table Registro 
( 
  NumCompetidor numeric(4) not null,
  Nombre varchar(20),
  FechaNacimiento DateTime,
  ApellidoPaterno varchar(20),
  ApellidoMaterno varchar(20),
  Prueba1 varchar(20),
  Prueba2 varchar(20),
  Prueba3 varchar(20),
  Genero varchar(10)
  
) 

insert into registro values (1054, 'Arantza', 'Martinez', 'Jimenez', '100mts', 'Longitud', '100c/v'); 
insert into registro values (0032, 'José', 'Ramirez', 'Cabrera', '400c/v', '1000mts', 'Jabalina', 'Hombre'); 
insert into registro values (0034, 'Mario', 'Perez', 'Cornejo', '200mts', '110c/v', 'Longitud', 'Hombre'); 
insert into registro values (0078, 'Estefania', 'López', 'Chávez', 'Disco', 'Bala', 'Martillo', 'Mujer'); 
insert into registro values (0012, 'Fernanda', 'González', 'Flores', '400mts', '800mts', '4x400mts', 'Mujer'); 
insert into registro values (0123, 'Raúl', 'Rodriguez', 'Carrasco', 'Altura', 'Garrocha', '110c/v', 'Hombre'); 