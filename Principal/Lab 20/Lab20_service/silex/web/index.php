<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app->get('/hello/{name}', function ($name) {
    return 'Hello '.$name.'!';
});

$app->get('/hello', function () {
    return 'Hello world!';
});

$app->run();
