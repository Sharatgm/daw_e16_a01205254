<!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
      <link rel="stylesheet" type= "text/css" href="../Stylesheet_DAW_BD.css"/>
    <link rel="stylesheet" type= "text/css" href="Stylesheet_laboratorio_11.css"/>
   <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
   
    
</head>
<body>
    
    <main>
        <section  class = "title_box2 title_background2">
            
            <!--- DROPDOWN MENU ------>
            
            <nav>
                <div class="dropdown">
                      <button class="dropbtn2"><i class="fa fa-bars w3-text-white w3-xxxlarge"></i></button>     
                      <div class="dropdown-content2">
                         <a href="DAW y BD.html">Inicio</a>
                        <a href="Lab 1/Laboratorio_1.html">HTML 5</a>
                        <a href="Lab 3/Laboratorio_3.html">CSS</a>
                        <a href="Lab 4/Laboratorio_4.html">JavaScript</a>
                        <a href="Lab 5/Laboratorio_5.html">Programación Orientada a Eventos</a>
                        <a href="Lab 6/Laboratorio_6.html">Documentos Dinámicos con JavaScript</a>
                        <a href="Lab 7/Laboratorio_7.html">Front-end Frameworks</a>
                        <a href="Lab 8/Laboratorio_8.php">Introducción a PHP</a>
                        <a href="Lab 11/Laboratorio_11.php">Formas con PHP y modelo en capas</a>
                        <a href="Lab 13/Formulario.php">Manejo de Sesiones PHP</a>
                         
                        <a href="Lab 14/index.php">PHP. Vistas dinámicas</a>
                          <a href="Lab 16/index.php">PHP. Manipulación registros</a>
                          <a href="Lab 17/index.php">Ajax</a>
                        <a href="Lab 20/Lab20_client/index.php">Servicios web REST</a>
                        
                        <a href="BD/Investiacion_1.html">Investigación 1</a>
                        <a href="Cuestionario_BD_DBMS/Cuestionario_BD_DBMS.html">Cuestionario BD &amp DBMS</a>
                      </div>
                    </div>
            </nav> 
            
            <!--------TITLE--------->
            
            <div class="title_square2">
                <h1 class = "title fadeInDown">Documentos dinámicos Js</h1>
            </div>
        </section>    
        <section>
            <div class = "descripcion">
                <h1>Descripción</h1>
                <p>En esta actividad veremos cómo manejar las formas html con php, y haremos una pequeña introducción al modelo de capas.</p>
            </div>
        </section>
         <section>
            <div id = "reloj" class = "reloj">
            </div>
            <div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        </section>
           <?php
            // define variables and set to empty values
            $name = $email = $gender = $telefono = $apellidoM = $apellidoP = $nacimiento = $nameErr = $genderErr = $emailErr = $websiteErr = $website = $nacimientoErr = ""; 
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                
                if (empty($_POST["name"])) {
                 $nameErr = "El nombre es obligatorio";
                } else {
                 $name = test_input($_POST["name"]);
                 // check if name only contains letters and whitespace
                 if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
                   $nameErr = "Solo se permiten letras y espacios en blanco"; 
                 }
                }
       
                if (empty($_POST["a_paterno"])) {
                    $apellidoP = "";
                   } else {
                    $apellidoP = test_input($_POST["a_paterno"]);
                    }
                    
              if (empty($_POST["a_materno"])) {
                    $apellidoM = "";
                   } else {
                    $apellidoM = test_input($_POST["a_materno"]);
                    }
            
                  if (empty($_POST["nacimiento"])) {
                     $nacimientoErr = "La fecha de nacimiento es obligatoria";
                   } else {
                     $nacimiento = test_input($_POST["nacimiento"]);
                    }
           
                 
         
              if (empty($_POST["gender"])) {
                     $genderErr = "El genero es obligatorio";
                   } else {
                     $gender = test_input($_POST["gender"]);
                    }
                 
             
               if (empty($_POST["email"])) {
                     $emailErr = "El correo es obligatorio";
                   } else {
                     $email = test_input($_POST["email"]);
                }
                
                  if (empty($_POST["telefono"])) {
                     $telefono = "";
                   } else {
                     $telefono = test_input($_POST["telefono"]);
                }
               
            }

            function test_input($data) {
               $data = trim($data);
               $data = stripslashes($data);
               $data = htmlspecialchars($data);
               return $data;
            }
          
        
        ?>
        <section>
            <div class = "formulario">
               
                    <h1>Formulario</h1>
                   
                    
	                <div id = "izq">
                        <p class = "required">*Campos requeridos</p>
                       <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
                            
                            <p>Nombres</p>
                            <input type="text" name="name"> <a title="Ingresa tu nombre, en caso de tener dos ingresa ambos." class="ayuda"><i class = "fa fa-question-circle w3-xlarge icon"></i></a>
                             <span class="error">* <?php echo $nameErr;?></span>
                            
                             <p>Apellido Paterno</p>
                            <input type="text" name="a_paterno">
             
                            
                             <p>Apellido Materno</p>
                            <input type="text" name="a_materno">
                            
                            <p>Género *       <span class="error"> <?php echo $genderErr;?></span></p>
                            <input type="checkbox" name="gender"  value ="Hombre">Hombre 
                            <br>
                            <input type="checkbox" name="gender" value= "Mujer">Mujer
                     
                            
                            <p>Fecha de Nacimiento</p>
                            <input type = "date" name="nacimiento">
                            <span class="error">* <?php echo $nacimientoErr;?></span>
                            
                            <p>Correo Tec</p>
                            <input type = "email" name = "email"> <a title="Matricula@itesm.mx Esta información es necesaria para ponernos en contacto contigo." class="ayuda"><i class = "fa fa-question-circle w3-xlarge icon"></i></a>
                            <span class="error">* <?php echo $emailErr;?></span>    
                            
                            <p>Teléfono</p><input type = "number" name = "telefono"> <a title="En caso de no encontrarte por medio del correo te llamaremos al número que ingreses aquí. No es obligatorio." class="ayuda"><i class = "fa fa-question-circle w3-xlarge icon"></i></a>
                            
                            <br><br>
                            <input type="submit" name="submit" value="Submit" class = "button2" >
                            
                        </form>
                    </div>      
                            
            </div>
        
        </section>
        <button class = "personalData" onclick="showDiv()">Personal Data</button>
        
            <div class = "input" id = "input">
                
          <?php
                echo "<h2>" . "Welcome" . "</h2>";
                echo "Nombre: " . $name . " " . $apellidoP . " " . $apellidoM . "<br>";
                echo "Género: " . $gender . "<br>";
                echo "Fecha de nacimiento: " . $nacimiento . "<br>";
                echo "Correo: " . $email . "<br>";
                echo "Teléfono: " . $telefono; 
            
                ?>
            </div>
           
        <section>
            <div class = "preguntas">
                <h1><i class = "fa fa-question"></i>    Preguntas</h1>
                
                <h3>¿Por qué es una buena práctica separar el controlador de la vista?.</h3>
                <p> Porque con el código separado es más fácil realizar modificaciones en un futuro además de que optimizamos el código.
                   
                </p>
                
                <h3>Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?</h3>
                <p>$ASSERT Revisa si la afirmación realizada es verdaddera o falsa y actúa con respecto a ello<br>
                $COOKIES es usado para identificar a un usuario y se envía cada vez que se solicita una página.</p>
                
                <h3>Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.</h3>
                <p>Todos los métodos relacionados con Date/ DateTime<br>
                Todos los métodos relacionados con image (imagedashedline(), imagefill(), imagefilter(), etc)</p>
                
       
            </div>
            
        </section>
        <section>
            <div class = "referencias">
             <h1>REFERENCIAS</h1>
        <!--     <button class="button2" onclick = "display_ref()">REFERENCIAS</button>   -->
           
               <a>N.A. (S.F). Assert. 03 Marzo de 2016, de PHP Sitio web: http://php.net/manual/es/function.assert.php</a>
                <a>N.A. (S.F). Assert_options. 03 Marzo de 2016, de PHP Sitio web: http://php.net/manual/es/function.assert-options.php</a> <br>
                <a>N.A. (S.F.). PHP 5 Cookies. 03 Marzo de 2016, de w3Schools Sitio web: http://www.w3schools.com/php/php_cookies.asp</a>
                
                
            
            </div>
        </section>
    </main>
<script type = "text/javascript" src = "../Js_DAW_BD.js"></script>
<script type = "text/javascript" src = "Js_laboratorio_11.js"></script>
  
</body>
</html>
