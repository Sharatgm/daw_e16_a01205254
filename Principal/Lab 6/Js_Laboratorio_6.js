
/*-----------------------MARCA DE AGUA----------------------------------*/


function funcion_tiempo() {
    
    setTimeout(marcaAgua, 3000);
}

function marcaAgua() {
    document.getElementById("marca").style.visibility = "visible";
    
}


/*----------------------------------------ESTILOS---------------------------------------------*/

function estilo_titulo() {
        document.getElementById("titulo").style.fontWeight = "600";
    document.getElementById("titulo").style.color = "#01DFD7";
}

function estilo_instruccion() {
    document.getElementById("instruccion").style.fontSize = "20px";
    document.getElementById("instruccion").style.color = "#5FB404";
     
}

function estilo1() {
    document.getElementById("oracion1").style.fontStyle = "oblique";
    document.getElementById("oracion1").style.color = "#FF8000";
     
}
function estilo2() {
    document.getElementById("oracion2").style.visibility = "hidden";
    
}
function estilo3() {
    document.getElementById("oracion3").style.fontVariant = "small-caps";
    document.getElementById("oracion3").style.color = "#D358F7";
     
}
function estilo4() {
    document.getElementById("oracion4").style.fontSize = "10px";
    document.getElementById("oracion4").style.color = "#2ECCFA";
     
}
function estilo5() {
    document.getElementById("oracion5").style.fontSize = "40px";
    document.getElementById("oracion5").style.color = "#F4FA58";
     
}
function estilo6() {
    document.getElementById("oracion6").style.opacity = "0.6";
    document.getElementById("ioracion6").style.color = "#FF00BF";
     
}

/*-----------------------------------------SET INTERVAL--------------------------------*/


    var hora =  setInterval(function(){ getHour() }, 1000);


function getHour() {
    var d = new Date();
    var t = d.toLocaleTimeString();
    document.getElementById("reloj").innerHTML = t;
}

/*-------------------------------------------DRAG-------------------------------*/


function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

/*--------------------------------REFERENCIAS----------------------*/

/*function display_ref() {
    document.getElementById("ref").style.display = "block";
    
}  */