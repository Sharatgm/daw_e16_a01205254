<!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <link rel="stylesheet" type= "text/css" href="../Stylesheet_DAW_BD.css"/>
   <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type= "text/css" href="Stylesheet_laboratorio_13.css"/>
    <script src="../sweetalert-master/dist/sweetalert-dev.js"></script>
    <link rel="stylesheet" href="../sweetalert-master/dist/sweetalert.css">
   
</head>
<body>
    
    <main>
        <section  class = "title_box2 title_background2">
            
            <!--- DROPDOWN MENU ------>
            
            <nav>
                <div class="dropdown">
                      <button class="dropbtn2"><i class="fa fa-bars w3-text-white w3-xxxlarge"></i></button>     
                      <div class="dropdown-content2">
                       <a href="DAW y BD.html">Inicio</a>
                        <a href="Lab 1/Laboratorio_1.html">HTML 5</a>
                        <a href="Lab 3/Laboratorio_3.html">CSS</a>
                        <a href="Lab 4/Laboratorio_4.html">JavaScript</a>
                        <a href="Lab 5/Laboratorio_5.html">Programación Orientada a Eventos</a>
                        <a href="Lab 6/Laboratorio_6.html">Documentos Dinámicos con JavaScript</a>
                        <a href="Lab 7/Laboratorio_7.html">Front-end Frameworks</a>
                        <a href="Lab 8/Laboratorio_8.php">Introducción a PHP</a>
                        <a href="Lab 11/Laboratorio_11.php">Formas con PHP y modelo en capas</a>
                        <a href="Lab 13/Formulario.php">Manejo de Sesiones PHP</a>
                            
                        <a href="Lab 14/index.php">PHP. Vistas dinámicas</a>
                          <a href="Lab 16/index.php">PHP. Manipulación registros</a>
                          <a href="Lab 17/index.php">Ajax</a>
                        <a href="Lab 20/Lab20_client/index.php">Servicios web REST</a>
                        
                        <a href="BD/Investiacion_1.html">Investigación 1</a>
                        <a href="Cuestionario_BD_DBMS/Cuestionario_BD_DBMS.html">Cuestionario BD &amp DBMS</a>

                      </div>
                    </div>
            </nav> 
            
            <!--------TITLE--------->
            
            <div class="title_square2">
                <h1 class = "title fadeInDown">Manejo de Sesiones PHP</h1>
            </div>
        </section>    
        <section>
            <div class = "descripcion">
                <h1>Descripción</h1>
                <p>En esta actividad veremos cómo manejar sesiones, y subir archivos con php.</p>
            </div>
        </section>
        <section>
             <div class = "sesionPrueba"><h3>Sesión de Prueba:</h3>
                            Nombre de usuario: Usuario1 <br>
                            Contraseña: 12345
            </div>
            <div class= "datos_personales">
                
                <div class = "datos">
                    <h1>Inicia sesión</h1>
        <!--           <p class = "required">*Campos requeridos</p>
                    <form action = "Control.php" method = "post">
                            <p>Nombres</p>
                            <input type="text" name="nombre"> <a title="Ingresa tu nombre, en caso de tener dos ingresa ambos." class="ayuda"><i class = "fa fa-question-circle w3-xlarge icon"></i></a>
                            
                             <p>Apellido Paterno</p>
                            <input type="text" name="apellidop"> 
                            
                             <p>Apellido Materno</p>
                            <input type="text" name="apellidom">
                            
                            <p>Fecha de Nacimiento</p>
                            <input type = "date" name="nacimiento"> 
                            
                            <p>Correo Tec</p>
                            <input type = "email" name = "correo"> <a title="Matricula@itesm.mx Esta información es necesaria para ponernos en contacto contigo." class="ayuda"><i class = "fa fa-question-circle w3-xlarge icon"></i></a>
                            
                            
                             <p>Usuario</p>
                            <input type="text" name="usuario">
                        
                            <p>Contraseña</p><input type = "password" name = "contraseña"> <a title="En caso de no encontrarte por medio del correo te llamaremos al número que ingreses aquí. No es obligatorio." class="ayuda"><i class = "fa fa-question-circle w3-xlarge icon"></i></a>
                            
                            <br><br>
                            <input type="submit" id = "button" class = "button2">
                            
                        </form>  -->
                       
                        <form class = "iniciaSesion" name = "iniciaSesion" action = "Control.php" method = "post" enctype= "application/x-www-form-urlencoded">
                            <?php
                                error_reporting(E_ALL ^ E_NOTICE);
                                if($_GET["error"] =="si"){
                                    echo "<span>Verifica tus datos</span>";
                                }else {
                                    echo "Introduce tus datos";
                                }
                            ?>
                            <br><br>
                            Usuario <br><input type = "text" name = "nombre_usuario" /><br><br>
                            Contraseña <br> <input type = "password" name = "contraseña"/><br><br>
                            <input class = "enviar" type = "submit" nombre = "enviar" value = "Entrar"/>
                        </form>
                </div>
            </div>
        </section>
         <section>
            <div class = "preguntas">
                <h1><i class = "fa fa-question"></i>    Preguntas</h1>
                
                    <h3>¿Por qué es importante hacer un session_unset() y luego un session_destroy()?</h3>
                <p> 
                   Se utiliza session_unset para códigos antiguos que no usan $_SESSION, si si utiliza se tiene que poner unset() para poder destruir variables en especifico y liberar la información previo a destruirla. Si se quiere guardar la información del usuario no se utiliza unset().
                    
                </p>
                
                          
                <h3>¿Cuál es la diferencia entre una variable de sesión y una cookie?</h3>
                <p>La variable de sesión contiene la información del usuario y se almacena en el servidor a diferencia de una cookie que se almacena en el cliente para poder recordar sus datos al momento de iniciar sesión. </p>
                
                <h3>¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de archivos cuando sube una foto con el mismo nombre?</h3>
                <p> </p>
               
            </div>
            
        </section>
         <section>
            <div class = "referencias">
             <h1>REFERENCIAS</h1>
        <!--     <button class="button2" onclick = "display_ref()">REFERENCIAS</button>   -->
           
               <a>N.A. (S.F). Common Pitfalls. 07 Marzo de 2016, de PHP Sitio web: http://php.net/manual/en/features.file-upload.common-pitfalls.php</a>
                <a>N.A. (S.F). Session_unset. 07 Marzo de 2016, de PHP Sitio web: http://php.net/manual/es/function.session-unset.php</a> <br>
                 <a>N.A. (S.F). Session_destroy. 07 Marzo de 2016, de PHP Sitio web: http://php.net/manual/es/function.session-destroy.php</a> <br>
                <a>N.A. (S.F.). PHP 5 File Upload. 07 Marzo de 2016, de w3Schools Sitio web: http://www.w3schools.com/php/php_file_upload.asp</a>
                
                
            
            </div>
        </section>
    </main>
  <script type = "text/javascript" src = "../Js_DAW_BD.js"></script>
    <script type = "text/javascript" src = "Js_laboratorio_13.js"></script>
  
</body>
</html>